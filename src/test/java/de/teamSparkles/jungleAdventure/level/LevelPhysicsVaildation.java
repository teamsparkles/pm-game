package de.teamSparkles.jungleAdventure.level;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.InputStream;

import de.teamSparkles.engine.texture.Color;
import de.teamSparkles.engine.texture.Texture;
import de.teamSparkles.jungleAdventure.level.iceLevel.IceLevel;
import de.teamSparkles.jungleAdventure.level.jungle.JungleLevel;
import de.teamSparkles.jungleAdventure.level.testing.LevelTesting;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class LevelPhysicsVaildation {
	
	@Parameterized.Parameters
	public static InputStream[] textures() {
		return new InputStream[]{
				LevelTesting.class.getResourceAsStream("physics.png"),
				JungleLevel.class.getResourceAsStream("physics.png"), //known to be broken
				IceLevel.class.getResourceAsStream("physics.png")
		};
	}
	
	@Parameterized.Parameter
	public InputStream in;
	
	@After
	public void tearDown() throws Exception {
		in.close();
	}
	
	@Test
	public void testPhysicsMap() {
		Texture physics = Texture.load(in);
		for (int x = 0; x < physics.width; x++) {
			for (int y = 0; y < physics.height; y++) {
				try {
					Color pixel = physics.getPixel(x, y);
					assertEquals("red", 0, pixel.r);
					assertEquals("green", 0, pixel.g);
					assertEquals("blue", 0, pixel.b);
					assertThat("alpha", pixel.a, anyOf(equalTo((byte) 0), equalTo((byte) -1)));
				} catch (AssertionError e) {
					throw new AssertionError("pixel[" + x + ", " + y + "]", e);
				}
			}
		}
	}
	
}
