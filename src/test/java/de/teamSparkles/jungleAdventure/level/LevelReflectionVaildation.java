package de.teamSparkles.jungleAdventure.level;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.InputStream;

import de.teamSparkles.engine.texture.Color;
import de.teamSparkles.engine.texture.Texture;
import de.teamSparkles.jungleAdventure.level.testing.LevelTesting;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class LevelReflectionVaildation {
	
	@Parameterized.Parameters
	public static InputStream[] maps() {
		return new InputStream[]{
				LevelTesting.class.getResourceAsStream("layer+1Reflection.png")
		};
	}
	
	@Parameterized.Parameter
	public InputStream in;
	
	@After
	public void tearDown() throws Exception {
		in.close();
	}
	
	@Test
	public void testReflectionMap() {
		Texture reflection = Texture.load(in);
		for (int x = 0; x < reflection.width; x++) {
			for (int y = 0; y < reflection.height; y++) {
				try {
					Color pixel = reflection.getPixel(x, y);
					if (pixel.a != 0) {
						//has reflection
						assertThat("red", pixel.r, not(anyOf(equalTo((byte) 0), equalTo((byte) -1))));
						assertThat("green", pixel.g, not(anyOf(equalTo((byte) 0), equalTo((byte) -1))));
						assertThat("blue", pixel.b, not(equalTo((byte) 0)));
					}
				} catch (AssertionError e) {
					throw new AssertionError("pixel[" + x + ", " + y + "]", e);
				}
			}
		}
	}
	
}
