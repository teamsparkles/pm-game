package de.teamSparkles.jungleAdventure.level;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.InputStream;

import de.teamSparkles.engine.texture.Color;
import de.teamSparkles.engine.texture.Texture;
import de.teamSparkles.jungleAdventure.level.testing.LevelTesting;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class LevelDistortionVaildation {
	
	@Parameterized.Parameters
	public static InputStream[] maps() {
		return new InputStream[]{
				LevelTesting.class.getResourceAsStream("layer+1Distortion.png")
		};
	}
	
	@Parameterized.Parameter
	public InputStream in;
	
	@After
	public void tearDown() throws Exception {
		in.close();
	}
	
	@Test
	public void testDistortionMap() {
		Texture distortion = Texture.load(in);
		for (int x = 0; x < distortion.width; x++) {
			for (int y = 0; y < distortion.height; y++) {
				try {
					Color pixel = distortion.getPixel(x, y);
					if (pixel.a != 0) {
						//has reflection
						assertThat("red", pixel.r, equalTo((byte) 127));
						assertThat("green", pixel.g, equalTo((byte) 127));
					}
				} catch (AssertionError e) {
					throw new AssertionError("pixel[" + x + ", " + y + "]", e);
				}
			}
		}
	}
	
}
