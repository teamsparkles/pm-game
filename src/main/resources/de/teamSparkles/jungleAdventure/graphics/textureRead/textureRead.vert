#version 430 core
#extension GL_ARB_shading_language_420pack : require

const int[6] vertexIdToInputIndex = {
0, 1, 2,
0, 3, 2
};

uniform vec4[4] inputs;
uniform vec2[2] screenSize;

layout (location = 0) out vec2 texCoord;

void main() {
    vec4 i = inputs[vertexIdToInputIndex[gl_VertexID % 6]];
    texCoord = i.zw;
    gl_Position = vec4((i.xy - screenSize[0]) / (screenSize[1] - screenSize[0]) * 2 - 1, 0, 1);
}
