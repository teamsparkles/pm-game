#version 430 core
#extension GL_ARB_shading_language_420pack : require

layout (location = 0) in vec2 texCoord;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 outReflection;
layout(location = 2) out vec4 outDistortion;

layout (binding = 0) uniform sampler2D texColor;
layout (binding = 1) uniform sampler2D texReflection;
layout (binding = 2) uniform sampler2D texDistortion;

void main() {
    vec4 color = texture(texColor, texCoord);
    if (color.a < 0.05) {
        discard;
    }
    outColor = color;
    outReflection = vec4(texture(texReflection, texCoord).rgb, color.a);
    outDistortion = vec4(texture(texDistortion, texCoord).rgb, color.a);
}
