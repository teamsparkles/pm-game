#version 430 core
#extension GL_ARB_shading_language_420pack : require

layout(location = 0) out vec4 outColor;

layout (location = 0) in vec2 inTexCoord;

layout (binding = 0) uniform sampler2D texColor;
layout (binding = 1) uniform sampler2D texReflection;
layout (binding = 2) uniform sampler2D texDistortion;
layout (binding = 3) uniform sampler2D texDistortionRandomNormal;

uniform vec2 uniformRandomOffset1;
uniform vec2 uniformRandomOffset2;

const float constDistortionTexCoordMulitplier = 1.0 / 8.0;

vec2 textureNormal(sampler2D sampler, vec2 texCoord) {
    return normalize(texture(sampler, texCoord).rgb * 2 - 1).rg;
}

void main() {
    //info textures
    vec3 reflectionInfo = vec3(texture(texReflection, inTexCoord));
    vec3 distortionInfo = vec3(texture(texDistortion, inTexCoord));

    //distortion offset
    vec2 randomOffset = distortionInfo.b
    * textureNormal(texDistortionRandomNormal, (inTexCoord + uniformRandomOffset1) * constDistortionTexCoordMulitplier)
    * textureNormal(texDistortionRandomNormal, (inTexCoord + uniformRandomOffset2) * constDistortionTexCoordMulitplier);

    //direct color
    vec3 color = texture(texColor, inTexCoord + randomOffset).rgb;

    //blending reflection
    vec3 reflectionColor = texture(texColor, inTexCoord + (reflectionInfo.xy * 2 - 1) + randomOffset).rgb * reflectionInfo.b;
    if (reflectionInfo.b > 0) {
        color = color * (1-reflectionInfo.b) + reflectionColor * reflectionInfo.b;
    }

    outColor = vec4(color, 1);
}
