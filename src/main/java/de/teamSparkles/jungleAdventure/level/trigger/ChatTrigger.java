package de.teamSparkles.jungleAdventure.level.trigger;

import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import de.teamSparkles.engine.graphics.Gl2DTexture;
import de.teamSparkles.engine.graphics.GlTextureParam;
import de.teamSparkles.engine.vector.Vector2;
import de.teamSparkles.jungleAdventure.level.Frame;
import de.teamSparkles.jungleAdventure.level.Level;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL13.GL_CLAMP_TO_BORDER;


public class ChatTrigger implements Trigger {
	
	public static final GlTextureParam CHAT_TEXTURE_PARAM = new GlTextureParam()
			.setFilterMin(GL_LINEAR)
			.setFilterMag(GL_LINEAR)
			.setWrap(GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER);
	
	public Vector2 position;
	public float distance;
	public final ChatMessage[] chatMessages;
	public @Nullable Gl2DTexture[] chatMessagesLoaded;
	
	private boolean keyWasPressed;
	private int chatMessageId = -1;
	private Vector2 prevScreenSize;
	private Vector2 prevScreenPos;
	
	public ChatTrigger(Vector2 position, float distance, ChatMessage[] chatMessages) {
		this.position = position;
		this.distance = distance;
		this.chatMessages = chatMessages;
	}
	
	@Override
	public void update(Level level) {
		if (glfwGetKey(level.jungleAdventure.gameLoop.windowPointer(), GLFW_KEY_E) == GLFW_PRESS) {
			if (!keyWasPressed) {
				keyWasPressed = true;
				
				if (chatMessageId == -1) {
					if (chatMessages.length != 0 && Vector2.distance(level.player.position, position) < distance) {
						prevScreenSize = level.getScreenSize();
						prevScreenPos = level.getScreenPos();
						level.changeScreenSize(chatMessages[0].screenSize, chatMessages[0].screenPos, 0.5f);
						chatMessageId = 0;
						load();
					}
				} else if (chatMessageId == chatMessages.length - 1) {
					level.changeScreenSize(prevScreenSize, prevScreenPos, 0.5f);
					prevScreenSize = null;
					prevScreenPos = null;
					chatMessageId = -1;
					release();
				} else {
					chatMessageId++;
					ChatMessage chatMessage = chatMessages[chatMessageId];
					level.changeScreenSize(chatMessage.screenSize, chatMessage.screenPos, 0.5f);
				}
			}
		} else {
			keyWasPressed = false;
		}
	}
	
	@Override
	public float getDepth() {
		return 10000;
	}
	
	@Override
	public void render(Level level, Frame frame) {
		if (chatMessageId != -1) {
			level.jungleAdventure.shaderTextureRead.draw(
					new float[]{
							-1, -1, 0, 0,
							-1, 1, 0, 1,
							1, 1, 1, 1,
							1, -1, 1, 0
					},
					new float[]{
							-1, -1, 1, 1
					},
					Objects.requireNonNull(Objects.requireNonNull(chatMessagesLoaded)[chatMessageId]),
					null,
					null
			);
		}
	}
	
	public void load() {
		chatMessagesLoaded = Arrays.stream(chatMessages)
				.map(chatMessage -> Gl2DTexture.load(chatMessage.relative.getResourceAsStream(chatMessage.name), CHAT_TEXTURE_PARAM))
				.toArray(Gl2DTexture[]::new);
	}
	
	@Override
	public void release() {
		if (chatMessagesLoaded != null) {
			for (Gl2DTexture texture : chatMessagesLoaded)
				Objects.requireNonNull(texture).release();
			chatMessagesLoaded = null;
		}
	}
	
	public static class ChatMessage {
		
		public final Class relative;
		public final String name;
		public final Vector2 screenSize;
		public final Vector2 screenPos;
		
		public ChatMessage(Class relative, String name, Vector2 screenSize, Vector2 screenPos) {
			this.relative = relative;
			this.name = name;
			this.screenSize = screenSize;
			this.screenPos = screenPos;
		}
		
		public Gl2DTexture createTexture() {
			return Gl2DTexture.load(relative.getResourceAsStream(name), CHAT_TEXTURE_PARAM);
		}
	}
	
	public static int[] randomIndex(int length) {
		List<Integer> ret = IntStream.range(0, length)
				.boxed()
				.collect(Collectors.toList());
		Collections.shuffle(ret);
		return ret.stream()
				.mapToInt(Integer::intValue)
				.toArray();
	}
}
