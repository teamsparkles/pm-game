package de.teamSparkles.jungleAdventure.level.trigger;

import de.teamSparkles.engine.vector.Vector2;
import de.teamSparkles.jungleAdventure.level.Level;

public abstract class ProximityTrigger implements Trigger {
	
	public Vector2 position;
	public float distance;
	
	public ProximityTrigger(Vector2 position, float distance) {
		this.position = position;
		this.distance = distance;
	}
	
	@Override
	public void update(Level level) {
		float distance = Vector2.distance(level.player.position, position);
		if (distance < this.distance)
			onTrigger(level, distance);
	}
	
	public abstract void onTrigger(Level level, float distance);
	
}
