package de.teamSparkles.jungleAdventure.level.trigger;

import de.teamSparkles.jungleAdventure.level.Frame;
import de.teamSparkles.jungleAdventure.level.Level;
import de.teamSparkles.jungleAdventure.level.Renderable;

public interface Trigger extends Renderable {
	@Override
	default float getDepth() {
		return -2000;
	}
	
	@Override
	default void update(Level level) {
	
	}
	
	@Override
	default void render(Level level, Frame frame) {
	
	}
	
	@Override
	default void release() {
	
	}
}
