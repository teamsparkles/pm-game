package de.teamSparkles.jungleAdventure.level.trigger;

import de.teamSparkles.jungleAdventure.level.Level;

public abstract class VerticalScrollTrigger implements Trigger {
	
	public float boundry;
	public boolean lever;
	
	public VerticalScrollTrigger(float boundry) {
		this.boundry = boundry;
	}
	
	@Override
	public void update(Level level) {
		if (!lever && level.player.position.x > boundry) {
			lever = true;
			onTrigger(level, true);
		} else if (lever && level.player.position.x < boundry) {
			lever = false;
			onTrigger(level, false);
		}
	}
	
	public abstract void onTrigger(Level level, boolean toRight);
}
