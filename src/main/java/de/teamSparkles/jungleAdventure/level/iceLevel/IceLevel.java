package de.teamSparkles.jungleAdventure.level.iceLevel;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import de.teamSparkles.engine.graphics.DynamicallyUploadedTexture;
import de.teamSparkles.engine.graphics.GlTextureParam;
import de.teamSparkles.engine.texture.Texture;
import de.teamSparkles.engine.vector.Vector2;
import de.teamSparkles.jungleAdventure.JungleAdventure;
import de.teamSparkles.jungleAdventure.level.Level;
import de.teamSparkles.jungleAdventure.level.Renderable;
import de.teamSparkles.jungleAdventure.level.RenderedLayer;
import de.teamSparkles.jungleAdventure.level.iceLevel.conversations.IceLevelConverations;
import de.teamSparkles.jungleAdventure.level.trigger.ChatTrigger;
import de.teamSparkles.jungleAdventure.level.trigger.ChatTrigger.ChatMessage;

import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL12C.GL_CLAMP_TO_EDGE;

public class IceLevel extends Level {
	
	private static final GlTextureParam LEVEL_TEXTURE_PARAM = new GlTextureParam()
			.setFilterMin(GL_LINEAR)
			.setFilterMag(GL_LINEAR)
			.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
	
	public static final Vector2 CHAT_SCREEN_SIZE = new Vector2(640, 360);
	public static final Vector2 ESKIMO_0 = new Vector2(4332, 575);
	public static final Vector2 ESKIMO_1 = new Vector2(14219, 653);
	public static final Vector2 ACTIVIST_0 = new Vector2(7863, 615);
	public static final Vector2 ACTIVIST_1 = new Vector2(11006, 740);
	
	private static final RandomChatMessage[][] CHAT_MESSAGES_ESKIMO = {
			{
					new RandomChatMessage("Eskimo1-1.png", false),
					new RandomChatMessage("Eskimo1-2.png", true),
					new RandomChatMessage("Eskimo1-3.png", false),
					new RandomChatMessage("Eskimo1-4.png", true),
			},
			{
					new RandomChatMessage("Eskimo2-1.png", false),
					new RandomChatMessage("Eskimo2-2.png", true),
					new RandomChatMessage("Eskimo2-3.png", false),
					new RandomChatMessage("Eskimo2-4.png", true),
			},
			{
					new RandomChatMessage("Eskimo3-1.png", false),
					new RandomChatMessage("Eskimo3-2.png", true),
					new RandomChatMessage("Eskimo3-3.png", false),
					new RandomChatMessage("Eskimo3-4.png", true),
			},
	};
	private static final RandomChatMessage[][] CHAT_MESSAGES_ACTIVIST = {
			{
					new RandomChatMessage("Aktivist1-1.png", false),
					new RandomChatMessage("Aktivist1-2.png", true),
					new RandomChatMessage("Aktivist1-3.png", false),
					new RandomChatMessage("Aktivist1-4.png", true),
			},
			{
					new RandomChatMessage("Aktivist2-1.png", false),
					new RandomChatMessage("Aktivist2-2.png", true),
			},
			{
					new RandomChatMessage("Aktivist3-1.png", false),
					new RandomChatMessage("Aktivist3-2.png", true),
					new RandomChatMessage("Aktivist3-3.png", false),
					new RandomChatMessage("Aktivist3-4.png", true),
			},
	};
	
	public static class RandomChatMessage {
		
		public final String fileName;
		public final boolean focusTrump;
		
		public RandomChatMessage(String fileName, boolean focusTrump) {
			this.fileName = fileName;
			this.focusTrump = focusTrump;
		}
	}
	
	public static IceLevel load(JungleAdventure jungleAdventure) {
		CompletableFuture<Texture> physics = CompletableFuture.supplyAsync(() -> Texture.load(IceLevel.class.getResourceAsStream("physics.png")));
		CompletableFuture<DynamicallyUploadedTexture> background = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(IceLevel.class.getResourceAsStream("background.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerM1 = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(IceLevel.class.getResourceAsStream("layer-1.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerP1 = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(IceLevel.class.getResourceAsStream("layer+1.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerP3 = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(IceLevel.class.getResourceAsStream("layer+3.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerP4 = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(IceLevel.class.getResourceAsStream("layer+4.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerP4Reflection = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(IceLevel.class.getResourceAsStream("layer+4Reflection.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerP4Distortion = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(IceLevel.class.getResourceAsStream("layer+4Distortion.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerP5 = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(IceLevel.class.getResourceAsStream("layer+5.png"), LEVEL_TEXTURE_PARAM));
		
		int[] activistRandomIndex = ChatTrigger.randomIndex(CHAT_MESSAGES_ACTIVIST.length);
		int[] eskimoRandomIndex = ChatTrigger.randomIndex(CHAT_MESSAGES_ESKIMO.length);
		
		try {
			return new IceLevel(
					jungleAdventure,
					new Vector2(15000, 2642),
					new Vector2(1440, 820),
					new Vector2(100, 620),
					physics.get(),
					new Renderable[]{
							new RenderedLayer(-1000, 1f,
									background.get(),
									null,
									null
							),
							new RenderedLayer(-1, 1f,
									layerM1.get(),
									null,
									null
							),
							new RenderedLayer(+1, 1f,
									layerP1.get(),
									null,
									null
							),
							new RenderedLayer(+3, new Vector2(1.25f, 1f),
									layerP3.get(),
									null,
									null
							),
							new RenderedLayer(+4, new Vector2(1.5f, 1f),
									layerP4.get(),
									layerP4Reflection.get(),
									layerP4Distortion.get()
							),
							new RenderedLayer(+5, new Vector2(1.75f, 1f),
									layerP5.get(),
									null,
									null
							),
							new ChatTrigger(
									ACTIVIST_0,
									500,
									createChatMessage(CHAT_MESSAGES_ACTIVIST, activistRandomIndex[0], ACTIVIST_0)
							),
							new ChatTrigger(
									ACTIVIST_1,
									500,
									createChatMessage(CHAT_MESSAGES_ACTIVIST, activistRandomIndex[1], ACTIVIST_1)
							),
							new ChatTrigger(
									ESKIMO_0,
									500,
									createChatMessage(CHAT_MESSAGES_ESKIMO, eskimoRandomIndex[0], ESKIMO_0)
							),
							new ChatTrigger(
									ESKIMO_1,
									500,
									createChatMessage(CHAT_MESSAGES_ESKIMO, eskimoRandomIndex[1], ESKIMO_1)
							)
					}
			);
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException(e);
		}
	}
	
	public IceLevel(JungleAdventure jungleAdventure, Vector2 size, Vector2 screenSize, Vector2 start, Texture physics, Renderable[] layers) {
		super(jungleAdventure, size, screenSize, start, physics, layers);
	}
	
	public static ChatMessage[] createChatMessage(RandomChatMessage[][] msgs, int index, Vector2 screenPos) {
		return Arrays.stream(msgs[index])
				.map(m -> new ChatMessage(IceLevelConverations.class, m.fileName, CHAT_SCREEN_SIZE, m.focusTrump ? null : screenPos))
				.toArray(ChatMessage[]::new);
	}
}
