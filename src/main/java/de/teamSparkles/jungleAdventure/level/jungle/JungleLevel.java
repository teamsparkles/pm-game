package de.teamSparkles.jungleAdventure.level.jungle;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import de.teamSparkles.engine.graphics.DynamicallyUploadedTexture;
import de.teamSparkles.engine.graphics.GlTextureParam;
import de.teamSparkles.engine.texture.Texture;
import de.teamSparkles.engine.vector.Vector2;
import de.teamSparkles.jungleAdventure.JungleAdventure;
import de.teamSparkles.jungleAdventure.level.Level;
import de.teamSparkles.jungleAdventure.level.Renderable;
import de.teamSparkles.jungleAdventure.level.RenderedLayer;
import de.teamSparkles.jungleAdventure.level.iceLevel.IceLevel;
import de.teamSparkles.jungleAdventure.level.jungle.conversations.JungleLevelConversations;
import de.teamSparkles.jungleAdventure.level.trigger.ChatTrigger;
import de.teamSparkles.jungleAdventure.level.trigger.VerticalScrollTrigger;

import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL12C.GL_CLAMP_TO_EDGE;

public class JungleLevel extends Level {
	
	private static final GlTextureParam LEVEL_TEXTURE_PARAM = new GlTextureParam()
			.setFilterMin(GL_LINEAR)
			.setFilterMag(GL_LINEAR)
			.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
	
	public static final Vector2 CHAT_SCREEN_SIZE = new Vector2(640, 360);
	public static final Vector2 FARMER_0 = new Vector2(11859, 135);
	public static final Vector2 ACTIVIST_0 = new Vector2(3387, 1226);
	public static final Vector2 ACTIVIST_1 = new Vector2(7663, 136);
	
	private static final RandomChatMessage[][] CHAT_MESSAGES_ACTIVIST = {
			{
					new RandomChatMessage("activist-1-1.png", false),
					new RandomChatMessage("activist-1-2.png", true),
					new RandomChatMessage("activist-1-3.png", false),
					new RandomChatMessage("activist-1-4.png", true),
			},
			{
					new RandomChatMessage("activist-2-1.png", false),
					new RandomChatMessage("activist-2-2.png", true),
					new RandomChatMessage("activist-2-3.png", false),
					new RandomChatMessage("activist-2-4.png", true),
			},
			{
					new RandomChatMessage("activist-3-1.png", false),
					new RandomChatMessage("activist-3-2.png", true),
					new RandomChatMessage("activist-3-3.png", false),
					new RandomChatMessage("activist-3-4.png", true),
			},
			{
					new RandomChatMessage("activist-4-1.png", false),
					new RandomChatMessage("activist-4-2.png", true),
					new RandomChatMessage("activist-4-3.png", false),
					new RandomChatMessage("activist-4-4.png", true),
			},
	};
	private static final RandomChatMessage[][] CHAT_MESSAGES_FARMER = {
			{
					new RandomChatMessage("farmer-1-1.png", false),
					new RandomChatMessage("farmer-1-2.png", true),
			},
			{
					new RandomChatMessage("farmer-2-1.png", false),
					new RandomChatMessage("farmer-2-2.png", true),
					new RandomChatMessage("farmer-2-3.png", false),
					new RandomChatMessage("farmer-2-4.png", true),
			},
			{
					new RandomChatMessage("farmer-3-1.png", false),
					new RandomChatMessage("farmer-3-2.png", true),
					new RandomChatMessage("farmer-3-3.png", false),
					new RandomChatMessage("farmer-3-4.png", true),
			},
	};
	
	public static class RandomChatMessage {
		
		public final String fileName;
		public final boolean focusTrump;
		
		public RandomChatMessage(String fileName, boolean focusTrump) {
			this.fileName = fileName;
			this.focusTrump = focusTrump;
		}
	}
	
	public static JungleLevel load(JungleAdventure jungleAdventure) {
		CompletableFuture<Texture> physics = CompletableFuture.supplyAsync(() -> Texture.load(JungleLevel.class.getResourceAsStream("physics.png")));
		CompletableFuture<DynamicallyUploadedTexture> background = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(JungleLevel.class.getResourceAsStream("background.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerM2 = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(JungleLevel.class.getResourceAsStream("layer-2.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerM1 = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(JungleLevel.class.getResourceAsStream("layer-1.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerP1 = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(JungleLevel.class.getResourceAsStream("layer+1.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerP1ref = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(JungleLevel.class.getResourceAsStream("reflection.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerP1dist = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(JungleLevel.class.getResourceAsStream("distortion.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerP2 = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(JungleLevel.class.getResourceAsStream("layer+2.png"), LEVEL_TEXTURE_PARAM));
		
		int[] activistRandomIndex = ChatTrigger.randomIndex(CHAT_MESSAGES_ACTIVIST.length);
		int[] farmerRandomIndex = ChatTrigger.randomIndex(CHAT_MESSAGES_FARMER.length);
		
		try {
			return new JungleLevel(
					jungleAdventure,
					new Vector2(15000, 1500),
					new Vector2(1280 * 1500f / 720, 1500),
					new Vector2(100, 300),
					physics.get(),
					new Renderable[]{
							new RenderedLayer(-1000, 0f,
									background.get(),
									null,
									null
							),
							new RenderedLayer(-2, 0.75f,
									layerM2.get(),
									null,
									null
							),
							new RenderedLayer(-1, 1f,
									layerM1.get(),
									null,
									null
							),
							new RenderedLayer(+1, 1f,
									layerP1.get(),
									layerP1ref.get(),
									layerP1dist.get()
							),
							new RenderedLayer(+2, 1.25f,
									layerP2.get(),
									null,
									null
							),
							new ChatTrigger(
									ACTIVIST_0,
									500,
									createChatMessage(CHAT_MESSAGES_ACTIVIST, activistRandomIndex[0], ACTIVIST_0)
							),
							new ChatTrigger(
									ACTIVIST_1,
									500,
									createChatMessage(CHAT_MESSAGES_ACTIVIST, activistRandomIndex[1], ACTIVIST_1)
							),
							new ChatTrigger(
									FARMER_0,
									500,
									createChatMessage(CHAT_MESSAGES_FARMER, farmerRandomIndex[0], FARMER_0)
							),
							new VerticalScrollTrigger(13700) {
								@Override
								public void onTrigger(Level level, boolean toRight) {
									if (toRight)
										level.jungleAdventure.loadLevel(IceLevel::load);
								}
							}
					}
			);
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException(e);
		}
	}
	
	public JungleLevel(JungleAdventure jungleAdventure, Vector2 size, Vector2 screenSize, Vector2 start, Texture physics, Renderable[] layers) {
		super(jungleAdventure, size, screenSize, start, physics, layers);
	}
	
	public static ChatTrigger.ChatMessage[] createChatMessage(RandomChatMessage[][] msgs, int index, Vector2 screenPos) {
		return Arrays.stream(msgs[index])
				.map(m -> new ChatTrigger.ChatMessage(JungleLevelConversations.class, m.fileName, CHAT_SCREEN_SIZE, m.focusTrump ? null : screenPos))
				.toArray(ChatTrigger.ChatMessage[]::new);
	}
}
