package de.teamSparkles.jungleAdventure.level;

import de.teamSparkles.engine.vector.Vector2;

import static org.lwjgl.glfw.GLFW.*;

public class Player implements Renderable {
	
	public static final float SPEED = 6.5f;
	public static final float JUMP_POWER = 20f;
	public static final float GRAVITY_ACCELERATION = 0.9f;
	
	public static final float MODEL_PIXEL_SIZE = 50f;
	public static final int[] COLLISION_FULL_BOX = new int[]{-20, -37, 20, 40};
	public static final int[] COLLISION_UPPER_BOX = new int[]{-20, -30, 20, 40};
	public static final int[] COLLISION_STEP_ASSIST_BOX = new int[]{-20, -37, 20, -30};
	public static final int[] COLLISION_ON_GROUND_BOX = new int[]{-20, -42, 20, -37};
	
	public Vector2 position;
	public float upwardMomentum;
	public boolean lookingLeft, isRunning;
	
	public Player(Vector2 position) {
		this.position = position;
	}
	
	@Override
	public void update(Level level) {
		long windowPointer = level.jungleAdventure.gameLoop.windowPointer();
		
		//input: sideways movement
		float sideways = 0;
		if (glfwGetKey(windowPointer, GLFW_KEY_A) == GLFW_PRESS || glfwGetKey(windowPointer, GLFW_KEY_LEFT) == GLFW_PRESS)
			sideways -= SPEED;
		if (glfwGetKey(windowPointer, GLFW_KEY_D) == GLFW_PRESS || glfwGetKey(windowPointer, GLFW_KEY_RIGHT) == GLFW_PRESS)
			sideways += SPEED;
		isRunning = sideways != 0;
		if (isRunning)
			lookingLeft = sideways < 0;
		
		//input: jumping up
		boolean jump = glfwGetKey(windowPointer, GLFW_KEY_W) == GLFW_PRESS || glfwGetKey(windowPointer, GLFW_KEY_UP) == GLFW_PRESS || glfwGetKey(windowPointer, GLFW_KEY_SPACE) == GLFW_PRESS;
		if (jump && collides(level, position, COLLISION_ON_GROUND_BOX))
			upwardMomentum = JUMP_POWER;
		else
			upwardMomentum -= GRAVITY_ACCELERATION;
		
		//vertical collision: bumping your head or hitting the ground
		Vector2 newPosVert = position.add(0, upwardMomentum);
		if (collides(level, newPosVert, COLLISION_FULL_BOX))
			upwardMomentum = 0;
		else
			position = newPosVert;
		
		//sideways collision: running against something
		Vector2 newPosHor = position.add(sideways, 0);
		int stepAssistUp = stepAssist(level, newPosHor, COLLISION_STEP_ASSIST_BOX);
		newPosHor = newPosHor.add(0, stepAssistUp);
		boolean collides = collides(level, newPosHor, COLLISION_UPPER_BOX);
		if (!collides)
			position = newPosHor;
	}
	
	private boolean collides(Level level, Vector2 position, int[] box) {
		for (int x = box[0]; x <= box[2]; x++) {
			for (int y = box[1]; y <= box[3]; y++) {
				int px = (int) (x + position.x);
				int py = (int) (y + position.y);
				if (px < 0 || py < 0 || px >= level.size.x || py >= level.size.y || level.physics.getPixel(px, py).a != 0) {
					return true;
				}
			}
		}
		return false;
	}
	
	private int stepAssist(Level level, Vector2 position, int[] box) {
		for (int y = box[3]; y >= box[1]; y--) {
			for (int x = box[0]; x <= box[2]; x++) {
				int px = (int) (x + position.x);
				int py = (int) (y + position.y);
				if (px < 0 || py < 0 || px >= level.size.x || py >= level.size.y || level.physics.getPixel(px, py).a != 0) {
					return y - box[1];
				}
			}
		}
		return 0;
	}
	
	@Override
	public float getDepth() {
		return 0;
	}
	
	@Override
	public void render(Level level, Frame frame) {
		Vector2 s = position.sub(frame.offset);
		level.draw(
				frame,
				new float[]{
						s.x - MODEL_PIXEL_SIZE, s.y - MODEL_PIXEL_SIZE, 0, 0,
						s.x - MODEL_PIXEL_SIZE, s.y + MODEL_PIXEL_SIZE, 0, 1,
						s.x + MODEL_PIXEL_SIZE, s.y + MODEL_PIXEL_SIZE, 1, 1,
						s.x + MODEL_PIXEL_SIZE, s.y - MODEL_PIXEL_SIZE, 1, 0
				},
				level.jungleAdventure.mainChar.getTexture(isRunning, lookingLeft),
				null,
				null
		);
	}
	
	@Override
	public void release() {
	
	}
}
