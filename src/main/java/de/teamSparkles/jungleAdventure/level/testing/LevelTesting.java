package de.teamSparkles.jungleAdventure.level.testing;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import de.teamSparkles.engine.graphics.DynamicallyUploadedTexture;
import de.teamSparkles.engine.graphics.GlTextureParam;
import de.teamSparkles.engine.texture.Texture;
import de.teamSparkles.engine.vector.Vector2;
import de.teamSparkles.jungleAdventure.JungleAdventure;
import de.teamSparkles.jungleAdventure.level.Level;
import de.teamSparkles.jungleAdventure.level.Renderable;
import de.teamSparkles.jungleAdventure.level.RenderedLayer;
import de.teamSparkles.jungleAdventure.level.trigger.VerticalScrollTrigger;

import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL12C.GL_CLAMP_TO_EDGE;

public class LevelTesting extends Level {
	
	private static final GlTextureParam LEVEL_TEXTURE_PARAM = new GlTextureParam()
			.setFilterMin(GL_LINEAR)
			.setFilterMag(GL_LINEAR)
			.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
	
	public static LevelTesting load(JungleAdventure jungleAdventure) {
		CompletableFuture<Texture> physics = CompletableFuture.supplyAsync(() -> Texture.load(LevelTesting.class.getResourceAsStream("physics.png")));
		CompletableFuture<DynamicallyUploadedTexture> background = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(LevelTesting.class.getResourceAsStream("background.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerM2 = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(LevelTesting.class.getResourceAsStream("layer-2.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerM1 = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(LevelTesting.class.getResourceAsStream("layer-1.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerP1 = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(LevelTesting.class.getResourceAsStream("layer+1.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerP1Distortion = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(LevelTesting.class.getResourceAsStream("layer+1Reflection.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerP1Reflection = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(LevelTesting.class.getResourceAsStream("layer+1Distortion.png"), LEVEL_TEXTURE_PARAM));
		CompletableFuture<DynamicallyUploadedTexture> layerP2 = CompletableFuture.supplyAsync(() -> DynamicallyUploadedTexture.load(LevelTesting.class.getResourceAsStream("layer+2.png"), LEVEL_TEXTURE_PARAM));
		
		try {
			return new LevelTesting(
					jungleAdventure,
					new Vector2(5000, 1080),
					new Vector2(1920, 1080),
					new Vector2(100, 250),
					physics.get(),
					new Renderable[]{
							new RenderedLayer(-1000, new Vector2(0, 1),
									background.get(),
									null,
									null
							),
							new RenderedLayer(-2, new Vector2(0.75f, 1),
									layerM2.get(),
									null,
									null
							),
							new RenderedLayer(-1, new Vector2(1, 1),
									layerM1.get(),
									null,
									null
							),
							new RenderedLayer(+1, new Vector2(1, 1),
									layerP1.get(),
									layerP1Distortion.get(),
									layerP1Reflection.get()
							),
							new RenderedLayer(+2, new Vector2(1.25f, 1),
									layerP2.get(),
									null,
									null
							),
							new VerticalScrollTrigger(2000) {
								@Override
								public void onTrigger(Level level, boolean toRight) {
									if (toRight) {
										level.changeScreenSize(new Vector2(1280, 720), new Vector2(2500, 540), 1);
									} else {
										level.changeScreenSize(new Vector2(1980, 1080), null, 1);
									}
								}
							}
					}
			);
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException(e);
		}
	}
	
	public LevelTesting(JungleAdventure jungleAdventure, Vector2 size, Vector2 screenSize, Vector2 start, Texture physics, Renderable[] layers) {
		super(jungleAdventure, size, screenSize, start, physics, layers);
	}
}
