package de.teamSparkles.jungleAdventure.level;

import de.teamSparkles.engine.vector.Vector2;

public class Frame {
	
	public final long timeNanos;
	public final Vector2 offset;
	public final Vector2 screenSize;
	public final Vector2 screenSizeDiv2;
	
	public Frame(long timeNanos, Vector2 offset, Vector2 screenSize, Vector2 screenSizeDiv2) {
		this.timeNanos = timeNanos;
		this.offset = offset;
		this.screenSize = screenSize;
		this.screenSizeDiv2 = screenSizeDiv2;
	}
}
