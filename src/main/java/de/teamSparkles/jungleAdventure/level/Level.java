package de.teamSparkles.jungleAdventure.level;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.stream.Stream;

import de.teamSparkles.engine.Releaseable;
import de.teamSparkles.engine.graphics.Gl2DTexture;
import de.teamSparkles.engine.texture.Texture;
import de.teamSparkles.engine.vector.Vector2;
import de.teamSparkles.jungleAdventure.JungleAdventure;

import static org.lwjgl.opengl.GL11.*;

public class Level implements Releaseable {
	
	public final @NotNull JungleAdventure jungleAdventure;
	public final @NotNull Vector2 size;
	public final @NotNull Vector2 start;
	public final @NotNull Texture physics;
	public final @NotNull Renderable[] layers;
	
	public Player player;
	
	//screenSize
	private long screenSizeChangeNanos;
	private float screenSizeChangeSpeed;
	private @NotNull Vector2 screenSizePrev;
	private @NotNull Vector2 screenSize;
	private @NotNull Vector2 screenPosPrev;
	private @Nullable Vector2 screenPos;
	
	public Level(@NotNull JungleAdventure jungleAdventure, @NotNull Vector2 size, @NotNull Vector2 screenSize, @NotNull Vector2 start, @NotNull Texture physics, Renderable[] layers) {
		this.jungleAdventure = jungleAdventure;
		this.size = size;
		this.start = start;
		this.physics = physics;
		
		this.player = new Player(start);
		this.layers = Stream.concat(Stream.of(player), Arrays.stream(layers)).sorted((o1, o2) -> Float.compare(o1.getDepth(), o2.getDepth())).toArray(Renderable[]::new);
		
		this.screenSizeChangeNanos = 0;
		this.screenSizePrev = screenSize;
		this.screenSize = screenSize;
		this.screenPosPrev = start;
		this.screenPos = null;
	}
	
	public void changeScreenSize(@NotNull Vector2 screenSize, @Nullable Vector2 screenPos, float speed) {
		long now = System.nanoTime();
		float factor = computeScreenSizeFactor(now);
		
		screenSizeChangeNanos = now;
		screenSizeChangeSpeed = speed;
		this.screenSizePrev = computeScreenSize(factor);
		this.screenSize = screenSize;
		this.screenPosPrev = computeScreenPos(factor);
		this.screenPos = screenPos;
	}
	
	public float computeScreenSizeFactor(long time) {
		float f = (time - screenSizeChangeNanos) / 1_000_000_000f / screenSizeChangeSpeed;
		if (f >= 1)
			return 1;
		if (f <= 0)
			return 0;
		//smoothstep
		return f * f * (3 - 2 * f);
	}
	
	public @NotNull Vector2 computeScreenSize(float factor) {
		return this.screenSizePrev.multiply(1 - factor).add(this.screenSize.multiply(factor));
	}
	
	public @NotNull Vector2 computeScreenPos(float factor) {
		return this.screenPosPrev.multiply(1 - factor).add((this.screenPos == null ? player.position : this.screenPos).multiply(factor));
	}
	
	public @NotNull Vector2 getScreenSize() {
		return screenSize;
	}
	
	public @Nullable Vector2 getScreenPos() {
		return screenPos;
	}
	
	public void update() {
		for (Renderable layer : layers)
			layer.update(this);
	}
	
	public @NotNull Frame render() {
		long now = System.nanoTime();
		float factor = computeScreenSizeFactor(now);
		Vector2 screenSize = computeScreenSize(factor);
		Vector2 screenSizeDiv2 = screenSize.divide(2);
		Vector2 screenPos = computeScreenPos(factor);
		
		Vector2 clampedPosition = Vector2.clamp(screenPos, screenSizeDiv2, size.sub(screenSizeDiv2));
		Frame frame = new Frame(now, clampedPosition, screenSize, screenSizeDiv2);
		
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		for (Renderable layer : layers)
			layer.render(this, frame);
		glDisable(GL_BLEND);
		
		return frame;
	}
	
	@Override
	public void release() {
		for (Renderable layer : layers)
			layer.release();
	}
	
	public void draw(@NotNull Frame frame, @NotNull float[] input, @NotNull Gl2DTexture texColor, @Nullable Gl2DTexture texReflection, @Nullable Gl2DTexture texDistortion) {
		jungleAdventure.shaderTextureRead.draw(input, new float[]{
				-frame.screenSizeDiv2.x, -frame.screenSizeDiv2.y, frame.screenSizeDiv2.x, frame.screenSizeDiv2.y
		}, texColor, texReflection, texDistortion);
	}
	
}
