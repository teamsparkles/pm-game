package de.teamSparkles.jungleAdventure.level;

import de.teamSparkles.engine.Releaseable;

public interface Renderable extends Releaseable {
	
	float getDepth();
	
	void update(Level level);
	
	void render(Level level, Frame frame);
	
	@Override
	void release();
}
