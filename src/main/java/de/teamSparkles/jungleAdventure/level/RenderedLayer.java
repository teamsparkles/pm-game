package de.teamSparkles.jungleAdventure.level;

import org.jetbrains.annotations.Nullable;

import de.teamSparkles.engine.graphics.DynamicallyUploadedTexture;
import de.teamSparkles.engine.graphics.Gl2DTexture;
import de.teamSparkles.engine.vector.Vector2;

public class RenderedLayer implements Renderable {
	
	public final float index;
	public final Vector2 speed;
	public final DynamicallyUploadedTexture texColor;
	public final @Nullable DynamicallyUploadedTexture texReflection;
	public final @Nullable DynamicallyUploadedTexture texDistortion;
	
	public RenderedLayer(int index, float speed, DynamicallyUploadedTexture texColor, @Nullable DynamicallyUploadedTexture texReflection, @Nullable DynamicallyUploadedTexture texDistortion) {
		this(index, new Vector2(speed, speed), texColor, texReflection, texDistortion);
	}
	
	public RenderedLayer(float index, Vector2 speed, DynamicallyUploadedTexture texColor, @Nullable DynamicallyUploadedTexture texReflection, @Nullable DynamicallyUploadedTexture texDistortion) {
		this.index = index;
		this.speed = speed;
		this.texColor = texColor;
		this.texReflection = texReflection;
		this.texDistortion = texDistortion;
	}
	
	@Override
	public float getDepth() {
		return index;
	}
	
	@Override
	public void update(Level level) {
	
	}
	
	public void render(Level level, Frame frame) {
		Vector2 startPixel = frame.offset.sub(frame.screenSizeDiv2).multiply(speed);
		Vector2 endPixel = startPixel.add(frame.screenSize);
		DynamicallyUploadedTexture.LoadedArea color = texColor.loadArea(startPixel.x, endPixel.x);
		//I'm ASSUMING that all DynamicallyUploadedTexture will have the same texCoords which REQUIRES their src texture to have the same size (and OVERSIZE too, but that's a constant)
		Gl2DTexture reflection = texReflection == null ? null : texReflection.loadArea(startPixel.x, endPixel.x).glTexture;
		Gl2DTexture distortion = texDistortion == null ? null : texDistortion.loadArea(startPixel.x, endPixel.x).glTexture;
		Vector2 start = color.texCoord(startPixel);
		Vector2 end = color.texCoord(endPixel);
		
		level.jungleAdventure.shaderTextureRead.draw(new float[]{
				-1, -1f, start.x, start.y,
				-1, 1f, start.x, end.y,
				1, 1f, end.x, end.y,
				1, -1f, end.x, start.y,
		}, new float[]{
				-1, -1, 1, 1
		}, color.glTexture, reflection, distortion);
	}
	
	public void release() {
		texColor.release();
		if (texReflection != null)
			texReflection.release();
		if (texDistortion != null)
			texDistortion.release();
	}
}
