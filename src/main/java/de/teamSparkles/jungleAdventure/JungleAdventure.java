package de.teamSparkles.jungleAdventure;

import org.lwjgl.system.MemoryUtil;

import java.util.function.Function;

import de.teamSparkles.engine.game.Game;
import de.teamSparkles.engine.game.GameBuilder;
import de.teamSparkles.engine.game.GameLoop;
import de.teamSparkles.engine.graphics.Gl2DTexture;
import de.teamSparkles.engine.graphics.GlFramebuffer;
import de.teamSparkles.engine.graphics.GlFramebuffer.Attachment;
import de.teamSparkles.engine.graphics.GlTextureParam;
import de.teamSparkles.engine.vector.Vector2;
import de.teamSparkles.jungleAdventure.graphics.postprocess.distortionReflection.PostprocessDistortion;
import de.teamSparkles.jungleAdventure.graphics.textureRead.ShaderTextureRead;
import de.teamSparkles.jungleAdventure.level.Frame;
import de.teamSparkles.jungleAdventure.level.Level;
import de.teamSparkles.jungleAdventure.level.jungle.JungleLevel;
import de.teamSparkles.jungleAdventure.sprites.mainChar.MainChar;

import static java.lang.Math.*;
import static org.lwjgl.opengl.GL43.*;

public class JungleAdventure extends Game {
	
	public static final boolean DO_GC_ON_LEVEL_CHANGE = true;
	
	public static final GlTextureParam TEXTURE_PARAM_TEXTURE = new GlTextureParam()
			.setFilterMin(GL_LINEAR)
			.setFilterMag(GL_LINEAR);
	public static final GlTextureParam TEXTURE_PARAM_FBO_ATTACHMENT = new GlTextureParam()
			.setFilterMin(GL_LINEAR)
			.setFilterMag(GL_LINEAR);
	
	public static void main(String[] args) {
		GameBuilder gameBuilder = new GameBuilder()
				.setTitle("Jungle Game")
				.setOpenglVersion(3, 2, false)
				.setStarter(JungleAdventure::new);
		if (args.length >= 2) {
			gameBuilder
					.setWindowed(Integer.parseInt(args[0]), Integer.parseInt(args[1]))
					.setOpenglDebugCallback((source, type, id, severity, length, message, userParam) -> System.out.println("OpenGL debug: " + MemoryUtil.memUTF8(message, length)));
		} else {
			gameBuilder
					.setFullscreen();
		}
		
		gameBuilder
				.build()
				.run();
	}
	
	public final GameLoop gameLoop;
	
	//vaoEmpty
	private final int vaoEmpty;
	
	//shaders
	public final ShaderTextureRead shaderTextureRead;
	
	//framebuffer
	public final Gl2DTexture fbo3dColor;
	public final Gl2DTexture fbo3dReflection;
	public final Gl2DTexture fbo3dDistortion;
	public final GlFramebuffer fbo3d;
	
	//parts
	public final MainChar mainChar;
	public final PostprocessDistortion postprocessDistortion;
	
	//level
	private Level level;
	
	public JungleAdventure(GameLoop gameLoop) {
		this.gameLoop = gameLoop;
		
		//vaoEmpty
		vaoEmpty = glGenVertexArrays();
		glBindVertexArray(vaoEmpty);
		
		//shaders
		shaderTextureRead = ShaderTextureRead.create();
		
		//framebuffer
		fbo3dColor = new Gl2DTexture(gameLoop.width, gameLoop.height, TEXTURE_PARAM_FBO_ATTACHMENT);
		fbo3dReflection = new Gl2DTexture(gameLoop.width, gameLoop.height, TEXTURE_PARAM_FBO_ATTACHMENT);
		fbo3dDistortion = new Gl2DTexture(gameLoop.width, gameLoop.height, TEXTURE_PARAM_FBO_ATTACHMENT);
		fbo3d = new GlFramebuffer(
				new Attachment(GL_COLOR_ATTACHMENT0, fbo3dColor),
				new Attachment(GL_COLOR_ATTACHMENT1, fbo3dReflection),
				new Attachment(GL_COLOR_ATTACHMENT2, fbo3dDistortion)
		);
		
		//parts
		mainChar = new MainChar();
		postprocessDistortion = new PostprocessDistortion();
		
		loadLevel(JungleLevel::load);
	}
	
	public void loadLevel(Function<JungleAdventure, Level> level) {
		if (this.level != null) {
			this.level.release();
			this.level = null;
		}
		
		if (DO_GC_ON_LEVEL_CHANGE) {
			for (int i = 0; i < 5; i++) {
				System.gc();
				System.runFinalization();
			}
		}
		this.level = level.apply(this);
	}
	
	public Level getLevel() {
		return level;
	}
	
	@Override
	public void release() {
		//vaoEmpty
		glDeleteVertexArrays(vaoEmpty);
		
		//shaders
		shaderTextureRead.release();
		
		//framebuffer
		fbo3d.release();
		fbo3dColor.release();
		fbo3dReflection.release();
		fbo3dDistortion.release();
		
		//parts
		mainChar.release();
		postprocessDistortion.release();
	}
	
	@Override
	public void update() {
		level.update();
	}
	
	@Override
	public void render() {
		fbo3d.bind();
		Frame frame = level.render();
		GlFramebuffer.unbind();
		
		float angle = System.nanoTime() / 1_000_000_000f * 2f * (float) PI / 40;
		float angle2 = angle + 34.6467f;
		Vector2 distortionOffset = frame.offset.divide(frame.screenSize);
		postprocessDistortion.draw(fbo3dColor, fbo3dReflection, fbo3dDistortion,
				new Vector2((float) sin(angle), (float) cos(angle)).add(distortionOffset),
				new Vector2((float) sin(angle2), (float) cos(angle2)).add(distortionOffset)
		);
	}
}
