package de.teamSparkles.jungleAdventure.graphics.textureRead;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.system.MemoryStack;

import java.io.IOException;

import de.teamSparkles.engine.graphics.Gl2DTexture;
import de.teamSparkles.engine.graphics.GlShader;

import static org.lwjgl.opengl.GL20.*;

public class ShaderTextureRead extends GlShader {
	
	public static ShaderTextureRead create() {
		try {
			return new ShaderTextureRead(
					readFromResource(ShaderTextureRead.class, "textureRead.vert"),
					readFromResource(ShaderTextureRead.class, "textureRead.frag")
			);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private final int uniformInputs;
	private final int uniformScreenSize;
	private final Gl2DTexture texReflectionDefault;
	private final Gl2DTexture texDistortionDefault;
	
	private ShaderTextureRead(String srcVertex, String srcFragment) {
		super(srcVertex, srcFragment);
		uniformInputs = glGetUniformLocation(shaderProgram, "inputs");
		uniformScreenSize = glGetUniformLocation(shaderProgram, "screenSize");
		texReflectionDefault = Gl2DTexture.singleColorTexture(0x7F7F00FF);
		texDistortionDefault = Gl2DTexture.singleColorTexture(0x7F7F00FF);
	}
	
	public void draw(@NotNull float[] input, @NotNull float[] screenArea, @NotNull Gl2DTexture texColor, @Nullable Gl2DTexture texReflection, @Nullable Gl2DTexture texDistortion) {
		bind();
		
		try {
			MemoryStack.stackPush();
			glUniform4fv(uniformInputs, MemoryStack.stackFloats(input));
			glUniform2fv(uniformScreenSize, MemoryStack.stackFloats(screenArea));
		} finally {
			MemoryStack.stackPop();
		}
		texColor.bind(0);
		(texReflection != null ? texReflection : texReflectionDefault).bind(1);
		(texDistortion != null ? texDistortion : texDistortionDefault).bind(2);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		unbind();
	}
	
}
