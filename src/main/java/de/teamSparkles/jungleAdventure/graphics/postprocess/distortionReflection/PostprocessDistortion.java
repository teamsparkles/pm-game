package de.teamSparkles.jungleAdventure.graphics.postprocess.distortionReflection;

import de.teamSparkles.engine.Releaseable;
import de.teamSparkles.engine.graphics.Gl2DTexture;
import de.teamSparkles.engine.graphics.GlTextureParam;
import de.teamSparkles.engine.texture.Texture;
import de.teamSparkles.engine.vector.Vector2;

import static org.lwjgl.opengl.GL11.*;

public class PostprocessDistortion implements Releaseable {
	
	public static final GlTextureParam TEXTURE_PARAM = new GlTextureParam()
			.setFilterMin(GL_LINEAR)
			.setFilterMag(GL_LINEAR)
			.setWrap(GL_REPEAT, GL_REPEAT);
	
	private final Gl2DTexture textureDistortionRandomNormal;
	
	//shaders
	private final ShaderPostprocessDistortion shaderDistortion;
	
	public PostprocessDistortion() {
		//textures
		textureDistortionRandomNormal = new Gl2DTexture(Texture.load(PostprocessDistortion.class.getResourceAsStream("distortionRandomNormal.png")), TEXTURE_PARAM);
		
		//shaders
		shaderDistortion = ShaderPostprocessDistortion.create();
	}
	
	public void draw(Gl2DTexture color, Gl2DTexture reflection, Gl2DTexture distortion, Vector2 randomOffset1, Vector2 randomOffset2) {
		shaderDistortion.draw(color, reflection, distortion, textureDistortionRandomNormal, randomOffset1, randomOffset2);
	}
	
	@Override
	public void release() {
		textureDistortionRandomNormal.release();
		shaderDistortion.release();
	}
}
