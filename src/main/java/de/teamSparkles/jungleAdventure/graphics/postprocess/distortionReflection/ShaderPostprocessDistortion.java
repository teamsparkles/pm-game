package de.teamSparkles.jungleAdventure.graphics.postprocess.distortionReflection;

import java.io.IOException;

import de.teamSparkles.engine.graphics.Gl2DTexture;
import de.teamSparkles.engine.graphics.GlShader;
import de.teamSparkles.engine.vector.Vector2;

import static org.lwjgl.opengl.GL20.*;

public class ShaderPostprocessDistortion extends GlShader {
	
	public static ShaderPostprocessDistortion create() {
		try {
			return new ShaderPostprocessDistortion(
					readFromResource(ShaderPostprocessDistortion.class, "distortion.vert"),
					readFromResource(ShaderPostprocessDistortion.class, "distortion.frag")
			);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private final int uniformRandomOffset1;
	private final int uniformRandomOffset2;
	
	private ShaderPostprocessDistortion(String srcVertex, String srcFragment) {
		super(srcVertex, srcFragment);
		uniformRandomOffset1 = glGetUniformLocation(shaderProgram, "uniformRandomOffset1");
		uniformRandomOffset2 = glGetUniformLocation(shaderProgram, "uniformRandomOffset2");
	}
	
	public void draw(Gl2DTexture color, Gl2DTexture reflection, Gl2DTexture distortion, Gl2DTexture distortionRandomNormal, Vector2 randomOffset1, Vector2 randomOffset2) {
		bind();
		glUniform2f(uniformRandomOffset1, randomOffset1.x, randomOffset1.y);
		glUniform2f(uniformRandomOffset2, randomOffset2.x, randomOffset2.y);
		color.bind(0);
		reflection.bind(1);
		distortion.bind(2);
		distortionRandomNormal.bind(3);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		unbind();
	}
	
}
