package de.teamSparkles.jungleAdventure.sprites.mainChar;

import java.util.Arrays;
import java.util.stream.IntStream;

import de.teamSparkles.engine.Releaseable;
import de.teamSparkles.engine.graphics.Gl2DTexture;
import de.teamSparkles.jungleAdventure.JungleAdventure;

public class MainChar implements Releaseable {
	
	public final Gl2DTexture[][][] textures;
	
	public MainChar() {
		textures = new Gl2DTexture[][][]
				{
						{
								IntStream.rangeClosed(0, 9).mapToObj(
										index -> Gl2DTexture.load(MainChar.class.getResourceAsStream("trump_idle_right_" + index + ".png"), JungleAdventure.TEXTURE_PARAM_TEXTURE)
								).toArray(Gl2DTexture[]::new),
								IntStream.rangeClosed(0, 9).mapToObj(
										index -> Gl2DTexture.load(MainChar.class.getResourceAsStream("trump_idle_left_" + index + ".png"), JungleAdventure.TEXTURE_PARAM_TEXTURE)
								).toArray(Gl2DTexture[]::new)
						},
						{
								IntStream.rangeClosed(0, 5).mapToObj(
										index -> Gl2DTexture.load(MainChar.class.getResourceAsStream("trump_run_right_" + index + ".png"), JungleAdventure.TEXTURE_PARAM_TEXTURE)
								).toArray(Gl2DTexture[]::new),
								IntStream.rangeClosed(0, 5).mapToObj(
										index -> Gl2DTexture.load(MainChar.class.getResourceAsStream("trump_run_left_" + index + ".png"), JungleAdventure.TEXTURE_PARAM_TEXTURE)
								).toArray(Gl2DTexture[]::new)
						}
				};
	}
	
	public Gl2DTexture getTexture(boolean isRunning, boolean lookingLeft) {
		Gl2DTexture[] animation = textures[isRunning ? 1 : 0][lookingLeft ? 1 : 0];
		return animation[(int) (System.nanoTime() * animation.length / 1_000_000_000L % animation.length)];
	}
	
	@Override
	public void release() {
		Arrays.stream(textures).flatMap(Arrays::stream).flatMap(Arrays::stream).forEach(Gl2DTexture::release);
	}
}
