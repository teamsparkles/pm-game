package de.teamSparkles.engine;

public interface Releaseable {
	
	void release();
}
