package de.teamSparkles.engine.vector;

public class Vector2 {
	
	public static final Vector2 ZERO = new Vector2(0, 0);
	
	public final float x, y;
	
	public Vector2(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public Vector2 add(float x, float y) {
		return new Vector2(
				this.x + x,
				this.y + y
		);
	}
	
	public Vector2 add(Vector2 vec) {
		return new Vector2(
				x + vec.x,
				y + vec.y
		);
	}
	
	public Vector2 sub(float x, float y) {
		return new Vector2(
				this.x - x,
				this.y - y
		);
	}
	
	public Vector2 sub(Vector2 vec) {
		return new Vector2(
				x - vec.x,
				y - vec.y
		);
	}
	
	public Vector2 multiply(float f) {
		return new Vector2(
				x * f,
				y * f
		);
	}
	
	public Vector2 negate() {
		return new Vector2(
				-x,
				-y
		);
	}
	
	public Vector2 multiply(Vector2 vec) {
		return new Vector2(
				x * vec.x,
				y * vec.y
		);
	}
	
	public Vector2 divide(float f) {
		return new Vector2(
				x / f,
				y / f
		);
	}
	
	public Vector2 divide(Vector2 vec) {
		return new Vector2(
				x / vec.x,
				y / vec.y
		);
	}
	
	public float length() {
		return (float) Math.sqrt(x * x + y * y);
	}
	
	public Vector2 normalize() {
		float l = length();
		return new Vector2(
				x / l,
				y / l
		);
	}
	
	public static float distance(Vector2 a, Vector2 b) {
		return a.sub(b).length();
	}
	
	public static Vector2 min(Vector2 a, Vector2 b) {
		return new Vector2(
				Math.min(a.x, b.x),
				Math.min(a.y, b.y)
		);
	}
	
	public static Vector2 max(Vector2 a, Vector2 b) {
		return new Vector2(
				Math.max(a.x, b.x),
				Math.max(a.y, b.y)
		);
	}
	
	public static Vector2 clamp(Vector2 vec, Vector2 min, Vector2 max) {
		return new Vector2(
				Math.max(min.x, Math.min(vec.x, max.x)),
				Math.max(min.y, Math.min(vec.y, max.y))
		);
	}
	
	@Override
	public String toString() {
		return "Vector2{" + x + ", " + y + '}';
	}
}
