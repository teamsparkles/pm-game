package de.teamSparkles.engine.graphics;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.nio.ByteBuffer;

import de.teamSparkles.engine.texture.Texture;
import de.teamSparkles.engine.vector.Vector2;

import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;
import static org.lwjgl.system.MemoryUtil.*;

public class DynamicallyUploadedTexture {
	
	public static final float OVERSIZE = 0.5f;
	public static final GlTextureParam TEXTURE_PARAM = new GlTextureParam()
			.setFilterMin(GL_LINEAR)
			.setFilterMag(GL_LINEAR)
			.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
	
	public static DynamicallyUploadedTexture load(InputStream in, GlTextureParam textureParam) {
		return new DynamicallyUploadedTexture(Texture.loadManual(in), textureParam);
	}
	
	public final Texture texture;
	public final GlTextureParam textureParam;
	
	private @Nullable LoadedArea loadedArea;
	
	public DynamicallyUploadedTexture(Texture texture, GlTextureParam textureParam) {
		this.texture = texture;
		this.textureParam = textureParam;
	}
	
	public @NotNull LoadedArea loadArea(float min, float max) {
		Gl2DTexture glTexture;
		if (loadedArea != null) {
			if (min >= loadedArea.min && Float.min(max, texture.width) <= loadedArea.max) {
				return loadedArea;
			}
			glTexture = loadedArea.glTexture;
		} else {
			glTexture = new Gl2DTexture(0, 0, TEXTURE_PARAM);
		}
		
		float minToMax = max - min;
		int localMin = Math.max((int) (min - minToMax * OVERSIZE), 0);
		int localMax = Math.min((int) (max + minToMax * OVERSIZE), texture.width);
		int localWidth = localMax - localMin;
		
		ByteBuffer local = memAlloc(texture.height * localWidth * 4);
		byte[] array = new byte[localWidth * 4];
		for (int y = 0; y < texture.height; y++) {
			texture.buffer.position(y * texture.width * 4 + localMin * 4);
			texture.buffer.get(array);
			local.position(y * localWidth * 4);
			local.put(array);
		}
		texture.buffer.rewind();
		local.rewind();
		
		Texture tempTexture = new Texture(local, localWidth, texture.height);
		glTexture.uploadTexture(tempTexture);
		memFree(local);
		
		return loadedArea = new LoadedArea(glTexture, localMin, localMax);
	}
	
	public void release() {
		texture.release();
		if (loadedArea != null)
			loadedArea.glTexture.release();
	}
	
	public class LoadedArea {
		
		public final Gl2DTexture glTexture;
		public final float min;
		public final float max;
		
		public LoadedArea(Gl2DTexture glTexture, float min, float max) {
			this.glTexture = glTexture;
			this.min = min;
			this.max = max;
		}
		
		public Vector2 texCoord(Vector2 vec) {
			float size = max - min;
			return new Vector2(
					(vec.x - min) / size,
					vec.y / texture.height
			);
		}
	}
	
}
