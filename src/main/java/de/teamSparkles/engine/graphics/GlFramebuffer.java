package de.teamSparkles.engine.graphics;

import java.util.Arrays;

import de.teamSparkles.engine.Releaseable;

import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL32.glFramebufferTexture;

public class GlFramebuffer implements Releaseable {
	
	private final int fboId;
	
	public GlFramebuffer(Attachment... attachments) {
		this.fboId = glGenFramebuffers();
		
		bind();
		for (Attachment attachment : attachments)
			glFramebufferTexture(GL_FRAMEBUFFER, attachment.attachment, attachment.texture.textureId, 0);
		//using this method incorrectly! Only fbos with tex in sequence work!
		glDrawBuffers(Arrays.stream(attachments).mapToInt(att -> att.attachment).toArray());
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			throw new RuntimeException("Framebuffer incomplete");
		unbind();
	}
	
	public void bind() {
		glBindFramebuffer(GL_FRAMEBUFFER, fboId);
	}
	
	public static void unbind() {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	
	@Override
	public void release() {
		glDeleteFramebuffers(fboId);
	}
	
	public static class Attachment {
		
		public final int attachment;
		public final Gl2DTexture texture;
		
		public Attachment(int attachment, Gl2DTexture texture) {
			this.attachment = attachment;
			this.texture = texture;
		}
	}
}
