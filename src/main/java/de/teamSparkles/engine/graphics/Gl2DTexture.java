package de.teamSparkles.engine.graphics;

import org.jetbrains.annotations.Nullable;
import org.lwjgl.system.MemoryStack;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.function.Supplier;

import de.teamSparkles.engine.Releaseable;
import de.teamSparkles.engine.texture.Texture;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL30.*;

public class Gl2DTexture implements Releaseable {
	
	public static Supplier<Gl2DTexture> loadWorker(InputStream in, GlTextureParam textureParam) {
		Texture tex = Texture.load(in);
		return () -> new Gl2DTexture(tex, textureParam);
	}
	
	public static Gl2DTexture load(InputStream in, GlTextureParam textureParam) {
		return new Gl2DTexture(Texture.load(in), textureParam);
	}
	
	public static Gl2DTexture singleColorTexture(int color) {
		try {
			MemoryStack stack = MemoryStack.stackPush();
			ByteBuffer buffer = stack.malloc(4);
			buffer.put(0, (byte) (color >> 24));
			buffer.put(1, (byte) (color >> 16));
			buffer.put(2, (byte) (color >> 8));
			buffer.put(3, (byte) (color));
			return new Gl2DTexture(1, 1, buffer,
					new GlTextureParam()
							.setFilterMin(GL_NEAREST)
							.setFilterMag(GL_NEAREST)
							.setWrap(GL_REPEAT, GL_REPEAT)
			);
		} finally {
			MemoryStack.stackPop();
		}
	}
	
	public int width, height;
	public final int textureId;
	
	public Gl2DTexture(int width, int height, GlTextureParam textureParam) {
		this(width, height, null, textureParam);
	}
	
	public Gl2DTexture(Texture texture, GlTextureParam textureParam) {
		this(texture.width, texture.height, texture.buffer, textureParam);
	}
	
	public Gl2DTexture(int width, int height, @Nullable ByteBuffer buffer, GlTextureParam textureParam) {
		this.textureId = glGenTextures();
		this.width = width;
		this.height = height;
		
		bind();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, textureParam.wrap[0]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, textureParam.wrap[1]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, textureParam.wrap[2]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, textureParam.filterMin);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, textureParam.filterMag);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
		if (buffer != null && textureParam.genMipmaps)
			glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
		unbind();
	}
	
	public void bind(int location) {
//		glEnable(GL_TEXTURE_2D);
		glActiveTexture(GL_TEXTURE0 + location);
		glBindTexture(GL_TEXTURE_2D, textureId);
	}
	
	public void bind() {
//		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, textureId);
	}
	
	public void unbind() {
		glBindTexture(GL_TEXTURE_2D, 0);
//		glDisable(GL_TEXTURE_2D);
	}
	
	public void uploadTexture(Texture texture) {
		this.width = texture.width;
		this.height = texture.height;
		bind();
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, texture.width, texture.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture.buffer);
		unbind();
	}
	
	@Override
	public void release() {
		glDeleteTextures(textureId);
	}
}
