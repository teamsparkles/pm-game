package de.teamSparkles.engine.game;

import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GLDebugMessageCallbackI;

import java.util.Objects;
import java.util.function.Function;

import static org.lwjgl.glfw.GLFW.*;

@SuppressWarnings("UnusedReturnValue")
public class GameBuilder implements Cloneable {
	
	public int width = 1920, height = 1080;
	public long monitor = 0;
	public String title = "Unnamed Game";
	public int openglVersionMayor = 2, openglVersionMinor = 1;
	public boolean openglForwardCompatible = false;
	public GLDebugMessageCallbackI openglDebugCallback;
	public Function<GameLoop, Game> starter;
	
	public GameBuilder() {
		glfwInit();
	}
	
	public GameBuilder setWindowed(int width, int height) {
		this.monitor = 0;
		this.width = width;
		this.height = height;
		return this;
	}
	
	public GameBuilder setFullscreen() {
		return setFullscreen(glfwGetPrimaryMonitor());
	}
	
	public GameBuilder setFullscreen(long monitor) {
		GLFWVidMode vidMode = Objects.requireNonNull(glfwGetVideoMode(monitor));
		return setFullscreen(monitor, vidMode.width(), vidMode.height());
	}
	
	public GameBuilder setFullscreen(long monitor, int width, int height) {
		this.monitor = monitor;
		this.width = width;
		this.height = height;
		return this;
	}
	
	public GameBuilder setTitle(String title) {
		this.title = title;
		return this;
	}
	
	public GameBuilder setStarter(Function<GameLoop, Game> starter) {
		this.starter = starter;
		return this;
	}
	
	public GameBuilder setOpenglVersion(int versionMayor, int versionMinor, boolean forwardCompatible) {
		this.openglVersionMayor = versionMayor;
		this.openglVersionMinor = versionMinor;
		this.openglForwardCompatible = forwardCompatible;
		return this;
	}
	
	public GameBuilder setOpenglDebugCallback(GLDebugMessageCallbackI openglDebugCallback) {
		this.openglDebugCallback = openglDebugCallback;
		return this;
	}
	
	public GameLoop build() {
		if (starter == null)
			throw new IllegalArgumentException("starter is null");
		return new GameLoop(this);
	}
}
