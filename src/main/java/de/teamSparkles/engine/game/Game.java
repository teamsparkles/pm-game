package de.teamSparkles.engine.game;

public abstract class Game {
	
	public void release() {
	
	}
	
	public void update() {
	
	}
	
	public abstract void render();
	
}
