package de.teamSparkles.engine.texture;

import java.util.Objects;

public class Color {
	
	public final byte r, g, b, a;
	
	public Color(byte r, byte g, byte b, byte a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	
	public Color(int r, int g, int b, int a) {
		this((byte) r, (byte) g, (byte) b, (byte) a);
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Color)) return false;
		Color color = (Color) o;
		return r == color.r &&
				g == color.g &&
				b == color.b &&
				a == color.a;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(r, g, b, a);
	}
	
	@Override
	public String toString() {
		return "Color{" +
				r +
				", " + g +
				", " + b +
				", " + a +
				'}';
	}
}
