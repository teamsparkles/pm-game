package de.teamSparkles.engine.texture;

import org.lwjgl.BufferUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import de.matthiasmann.twl.utils.PNGDecoder;

import static org.lwjgl.system.MemoryUtil.*;

public class Texture {
	
	public static Texture load(InputStream in) {
		return load(in, new Color(0, 0, 0, 0));
	}
	
	public static Texture load(InputStream in, Color borderColor) {
		try {
			PNGDecoder dec = new PNGDecoder(in);
			int width = dec.getWidth();
			int height = dec.getHeight();
			ByteBuffer buffer = BufferUtils.createByteBuffer(height * width * 4);
			dec.decodeFlipped(buffer, width * 4, PNGDecoder.Format.RGBA);
			buffer.flip();
			return new Texture(buffer, width, height, borderColor);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static Texture loadManual(InputStream in) {
		return loadManual(in, new Color(0, 0, 0, 0));
	}
	
	public static Texture loadManual(InputStream in, Color borderColor) {
		try {
			PNGDecoder dec = new PNGDecoder(in);
			int width = dec.getWidth();
			int height = dec.getHeight();
			ByteBuffer buffer = memAlloc(height * width * 4);
			dec.decodeFlipped(buffer, width * 4, PNGDecoder.Format.RGBA);
			buffer.flip();
			return new Texture(buffer, width, height, borderColor, true);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public final ByteBuffer buffer;
	public final int width, height;
	public final Color borderColor;
	public final boolean doManualRelease;
	
	public Texture(ByteBuffer buffer, int width, int height) {
		this(buffer, width, height, new Color(0, 0, 0, 0), false);
	}
	
	public Texture(ByteBuffer buffer, int width, int height, Color borderColor) {
		this(buffer, width, height, borderColor, false);
	}
	
	public Texture(ByteBuffer buffer, int width, int height, Color borderColor, boolean doManualRelease) {
		this.buffer = buffer;
		this.width = width;
		this.height = height;
		this.borderColor = borderColor;
		this.doManualRelease = doManualRelease;
	}
	
	public Color getPixel(int x, int y) {
		if (x < 0 || x >= width || y < 0 || y >= height)
			return borderColor;
		int index = (x + y * width) * 4;
		return new Color(buffer.get(index), buffer.get(index + 1), buffer.get(index + 2), buffer.get(index + 3));
	}
	
	public void setPixel(int x, int y, Color color) {
		if (x < 0 || x >= width || y < 0 || y >= height)
			return;
		int index = (x + y * width) * 4;
		buffer.put(index, color.r);
		buffer.put(index + 1, color.g);
		buffer.put(index + 2, color.b);
		buffer.put(index + 3, color.a);
	}
	
	public void release() {
		if (doManualRelease)
			memFree(buffer);
	}
	
}
